package com.example.emil.fileioexercise;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button loadFileButton = (Button) findViewById(R.id.loadFileButton);
        final Button saveDBButton = (Button) findViewById(R.id.saveDBButton);
        final Button loadDBButton = (Button) findViewById(R.id.loadDBButton);
        final Button calcMeanButton = (Button) findViewById(R.id.calcMeanButton);
        final Button saveFileButton = (Button) findViewById(R.id.saveFileButton);

        List<String> patientNames = new ArrayList<String>();
        List<Integer> timeStamps = new ArrayList<Integer>();
        List<Integer> HR = new ArrayList<Integer>();

        loadFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<String> patientNames = new ArrayList<String>();
                List<Integer> timeStamps = new ArrayList<Integer>();
                List<Integer> HR = new ArrayList<Integer>();

                InputStream is = getResources().openRawResource(R.raw.test);

                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                try {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        String[] RowData = line.split(";");
                        patientNames.add(RowData[0]);
                        timeStamps.add(Integer.parseInt(RowData[1]));
                        HR.add(Integer.parseInt(RowData[2]));
                    }
                }
                catch (IOException ex) {
                    Toast toast = Toast.makeText(getApplicationContext(), "ERROR: Could not read file", Toast.LENGTH_SHORT);
                }
                finally {
                    try {
                        is.close();
                    }
                    catch (IOException e) {
                        Toast toast = Toast.makeText(getApplicationContext(), "ERROR: Could not close file", Toast.LENGTH_SHORT);
                    }
                }


            }
        });

        saveDBButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        loadDBButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        calcMeanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        saveFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });



    }
}
